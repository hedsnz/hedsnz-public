from machine import Pin, PWM
from random import randint
from time import sleep

# Define the maximum brightness as the largest possible 16-bit value (as we are
# controlling the voltage with a 16-bit integer), and set the minimum brightness
# to half of this value (as we don't want the LED to be too dim at any point).
max_brightness = 65535
min_brightness = int(round(max_brightness / 2, 0))

# We want random noise not only in the brightness of the flickering, but also in
# the length of time that a given brightness is active. These are integers to be
# compatible with random.randint, but we will ultimately be dividing by 10,000
# when calling them, so 1000 and 1 correspond to 10 milliseconds and 10
# microseconds, respectively.
max_sleep_time = 1000
min_sleep_time = 1

# Instantiate the pulse-width modulation class for the GPIO pin connected to the
# LED. freq is frequency in Hz of the PWM cycle, which is basically how often to
# switch the power on and off. Probably anything over 60 Hz is fine, but 1000
# seems to be a common setting.
pwm = PWM(Pin(15), freq=1000)

# Set a random brightness, for a random period of time, and loop indefinitely.
while True:
    pwm.duty_u16(randint(min_brightness, max_brightness))
    sleep(randint(min_sleep_time, max_sleep_time) / 10000)
