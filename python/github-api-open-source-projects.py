import requests
import pandas as pd

# Define the API URL -- searching issues
url = "https://api.github.com/search/issues"

# Define a simple query: Looking at the R language
params = 'q=language:r'

# Make the API request
res = requests.get(url=url, params=params)

# Show the returned dictionary (with over 1 million issues identified)
res.json()

# Filter to only include open issues
params = 'q=language:r+state:open'
res = requests.get(url=url, params=params)
res.json()["total_count"]

# Filter to only include help-wanted tag
params = 'q=language:r+state:open+label:"help+wanted"'
res = requests.get(url=url, params=params)
res.json()["total_count"]

# Look at the keys of the first item
res.json()["items"][0].keys()

# Grab the interesting values of the first item
[res.json()["items"][0][x] for x in ['title', 'html_url', 'labels', 'updated_at']]

# Loop over returned objects to construct data frame for display
titles = []
urls = []
labels = []
updated = []

for i in res.json()["items"]:
    titles.append(i["title"])
    urls.append(i["html_url"])
    updated.append(i["updated_at"])
    these_labels = [x["name"] for x in i["labels"]]
    toadd = ", ".join(these_labels)
    labels.append(toadd)

tbl = pd.DataFrame(
        {
            "Title": titles,
            "URL": urls,
            "Labels": labels,
            "Updated": updated
        }
    ).sort_values("Updated", ascending=False, ignore_index=True)