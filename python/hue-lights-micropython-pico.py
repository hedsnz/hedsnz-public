# Control Philips Hue bedroom lights with a Raspberry Pi Pico microcontroller
# hooked up to a button.

# https://developers.home-assistant.io/docs/api/rest/
# Requires secrets.py in the same directory, with SSID, PASSWORD and TOKEN vars

from machine import Pin
from secrets import SSID, PASSWORD, TOKEN
import gc
import network
import requests
import rp2
import time

button = Pin(15, Pin.IN, Pin.PULL_UP)

class WiFi:
    """Just a small wrapper around network.WLAN"""

    def __init__(self, ssid, password):
        """Connect to the WiFi"""
        wlan = network.WLAN(network.STA_IF)
        wlan.active(True)
        wlan.connect(ssid, password)
        self.wlan = wlan

    def is_connected(self):
        """Basic back-off connection check"""
        retry_seconds = 0.5
        while not self.wlan.isconnected():
            if retry_seconds > 16:
                print("Failed to connect to WiFi.")
                return False
            print(f"Failed to connect, trying again in {retry_seconds} s...")
            time.sleep(retry_seconds)
            retry_seconds = retry_seconds * 2
        print("Connected to WiFi.")
        return True

wifi = WiFi(SSID, PASSWORD)


def get_api_data(token=TOKEN):
    """
    Get HTTP headers and base URL for the Home Assistant API call
    :param token: string of Home Assistant long-lived access token
    :return: dict with "headers" and "url" keys
    """
    headers = {
        "Authorization": f"Bearer {token}",
        "Content-Type": "application/json",
    }
    url = "http://homeassistant.local:8123"
    return({
        "headers": headers,
        "url": url,
    })


def toggle_light():
    """
    Toggle the light on or off.
    :return: return value of requests.post
    """
    endpoint = "/api/services/light/toggle"
    api_data = get_api_data()
    response = requests.post(
        url=api_data["url"] + endpoint,
        headers=api_data["headers"],
        data='{"entity_id": "light.bedroom"}'
    )
    return response


def toggle_scene():
    """
    Toggle the "scene", i.e., the light setting.
    There's no out-of-the-box way to get the current scene, so instead we get
    the state of the bedroom light, and use the 'brightness' attribute to
    infer the scene.
    :return: return value of requests.post
    """
    api_data = get_api_data()
    endpoint = "/api/services/hue/activate_scene"
    current_state = requests.get(
        url=api_data["url"] + "/api/states/light.bedroom",
        headers=api_data["headers"],
    )
    if current_state.json()["attributes"]["brightness"] > 240:
        scene = "scene.bedroom_relax"
    else:
        scene = "scene.bedroom_concentrate"
    response = requests.post(
        url=api_data["url"] + endpoint,
        headers=api_data["headers"],
        data=f'{{"entity_id": "{scene}"}}'
    )
    return response


if wifi.is_connected():
    while True:
        # Turn light on or off on main button press
        if button.value() == 0:
            res = toggle_light()
            if not res.status_code == 200:
                print(res.text)
        # Change light brightness on bootsel button press
        elif rp2.bootsel_button() == 1:
            res = toggle_scene()
            if not res.status_code == 200:
                print(res.text)
        time.sleep(0.1)
        gc.collect()
