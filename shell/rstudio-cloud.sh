# Create a Ubuntu 20.04 droplet on Digital Ocean
# using the cheapest settings to start with. Add this
# script to the user-data field when you create the
# droplet so that it runs on startup. Note that it
# can't have any interactive elements!


# Set up basic firewall
ufw allow OpenSSH
echo "y" | ufw enable

# Install R
# update indices
apt update -qq
# install two helper packages we need
apt install --no-install-recommends software-properties-common dirmngr
# add the signing key (by Michael Rutter) for these repos
# To verify key, run gpg --show-keys /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc 
# Fingerprint: 298A3A825C0D65DFD57CBB651716619E084DAB9
wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | sudo tee -a /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc
# add the R 4.0 repo from CRAN -- lsb_release should evaluate to "focal" for 20.04
add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran41/"
# install
apt install --no-install-recommends r-base -y

# install rstudio server
sudo apt-get install gdebi-core
wget https://download2.rstudio.org/server/bionic/amd64/rstudio-server-2022.02.1-461-amd64.deb
sudo gdebi rstudio-server-2022.02.1-461-amd64.deb -n

# add default user
/usr/sbin/useradd default
# add password
echo default:new_password | chpasswd